extends AudioStreamPlayer

@onready var go = preload("res://Assets/Announcer/Go.wav")
@onready var defeat = preload("res://Assets/Announcer/Defeat.wav")
@onready var shame = preload("res://Assets/Announcer/Shame.wav")
@onready var victory = preload("res://Assets/Announcer/Victory.wav")
@onready var getready = preload("res://Assets/Announcer/GetReady.wav")
@onready var victory2 = preload("res://Assets/Announcer/MercilessVictory.wav")
@onready var defeat2 = preload("res://Assets/Announcer/EmbarrassingDefeat.wav")
@onready var firstblood = preload("res://Assets/Announcer/FirstBlood.wav")
@onready var yousuck = preload("res://Assets/Announcer/YouSuck.wav")
@onready var payback = preload("res://Assets/Announcer/Payback.wav")
@onready var welcome = preload("res://Assets/Announcer/Welcome.wav")

func speak(sound) -> void:
	stream = sound
	play()
