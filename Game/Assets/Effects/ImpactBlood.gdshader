shader_type spatial;
varying float elapsed_time;

void vertex() {
	elapsed_time = TIME;
}

//---

float rand(vec2 x) {
    return fract(cos(mod(dot(x, vec2(13.9898, 8.141)), 3.14)) * 43758.5453);
}

vec2 rand2(vec2 x) {
    return fract(cos(mod(vec2(dot(x, vec2(13.9898, 8.141)),
						      dot(x, vec2(3.4562, 17.398))), vec2(3.14))) * 43758.5453);
}

vec3 rand3(vec2 x) {
    return fract(cos(mod(vec3(dot(x, vec2(13.9898, 8.141)),
							  dot(x, vec2(3.4562, 17.398)),
                              dot(x, vec2(13.254, 5.867))), vec3(3.14))) * 43758.5453);
}

vec3 rgb2hsv(vec3 c) {
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
	vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);

	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

instance uniform float o3007790_ANIM = 0.0; // {default:0.5, label:Animation, max:1, min:0, name:ANIM, step:0.01, type:named_parameter}
instance uniform float o3007790_OFFSET = 0.0; // {default:0.5, label:Offset, max:1, min:0, name:OFFSET, step:0.01, type:named_parameter}
float perlin(vec2 uv, vec2 size, int iterations, float persistence, int seed) {
	vec2 seed2 = rand2(vec2(float(seed), 1.0-float(seed)));
    float rv = 0.0;
    float coef = 1.0;
    float acc = 0.0;
    for (int i = 0; i < iterations; ++i) {
    	vec2 step = vec2(1.0)/size;
		vec2 xy = floor(uv*size);
        float f0 = rand(seed2+mod(xy, size));
        float f1 = rand(seed2+mod(xy+vec2(1.0, 0.0), size));
        float f2 = rand(seed2+mod(xy+vec2(0.0, 1.0), size));
        float f3 = rand(seed2+mod(xy+vec2(1.0, 1.0), size));
        vec2 mixval = smoothstep(0.0, 1.0, fract(uv*size));
        rv += coef * mix(mix(f0, f1, mixval.x), mix(f2, f3, mixval.x), mixval.y);
        acc += coef;
        size *= 2.0;
        coef *= persistence;
    }
    
    return rv / acc;
}
float shape_circle(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float distance = length(uv);
    return clamp((1.0-distance/size)/edge, 0.0, 1.0);
}

float shape_polygon(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = atan(uv.x, uv.y)+3.14159265359;
    float slice = 6.28318530718/sides;
    return clamp((1.0-(cos(floor(0.5+angle/slice)*slice-angle)*length(uv))/size)/edge, 0.0, 1.0);
}

float shape_star(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = atan(uv.x, uv.y);
    float slice = 6.28318530718/sides;
    return clamp((1.0-(cos(floor(angle*sides/6.28318530718-0.5+2.0*step(fract(angle*sides/6.28318530718), 0.5))*slice-angle)*length(uv))/size)/edge, 0.0, 1.0);
}

float shape_curved_star(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = max(edge, 1.0e-8);
    float angle = 2.0*(atan(uv.x, uv.y)+3.14159265359);
    float slice = 6.28318530718/sides;
    return clamp((1.0-cos(floor(0.5+0.5*angle/slice)*2.0*slice-angle)*length(uv)/size)/edge, 0.0, 1.0);
}

float shape_rays(vec2 uv, float sides, float size, float edge) {
    uv = 2.0*uv-1.0;
	edge = 0.5*max(edge, 1.0e-8)*size;
	float slice = 6.28318530718/sides;
    float angle = mod(atan(uv.x, uv.y)+3.14159265359, slice)/slice;
    return clamp(min((size-angle)/edge, angle/edge), 0.0, 1.0);
}

vec2 transform2_clamp(vec2 uv) {
	return clamp(uv, vec2(0.0), vec2(1.0));
}

vec2 transform2(vec2 uv, vec2 translate, float rotate, vec2 scale) {
 	vec2 rv;
	uv -= translate;
	uv -= vec2(0.5);
	rv.x = cos(rotate)*uv.x + sin(rotate)*uv.y;
	rv.y = -sin(rotate)*uv.x + cos(rotate)*uv.y;
	rv /= scale;
	rv += vec2(0.5);
	return rv;	
}vec2 rotate(vec2 uv, vec2 center, float rotate) {
 	vec2 rv;
	uv -= center;
	rv.x = cos(rotate)*uv.x + sin(rotate)*uv.y;
	rv.y = -sin(rotate)*uv.x + cos(rotate)*uv.y;
	rv += center;
    return rv;	
}// Based on https://www.shadertoy.com/view/ldl3W8
// The MIT License
// Copyright © 2013 Inigo Quilez
vec3 iq_voronoi(vec2 x, vec2 size, vec2 stretch, float randomness, vec2 seed) {
    vec2 n = floor(x);
    vec2 f = fract(x);

	vec2 mg, mr, mc;
    float md = 8.0;
    for (int j=-1; j<=1; j++)
    for (int i=-1; i<=1; i++) {
        vec2 g = vec2(float(i),float(j));
		vec2 o = randomness*rand2(seed + mod(n + g + size, size));
		vec2 c = g + o;
        vec2 r = c - f;
		vec2 rr = r*stretch;
        float d = dot(rr,rr);

        if (d<md) {
			mc = c;
            md = d;
            mr = r;
            mg = g;
        }
    }

    md = 8.0;
    for (int j=-2; j<=2; j++)
    for (int i=-2; i<=2; i++) {
        vec2 g = mg + vec2(float(i),float(j));
		vec2 o = randomness*rand2(seed + mod(n + g + size, size));
        vec2 r = g + o - f;
		vec2 rr = (mr-r)*stretch;
        if (dot(rr,rr)>0.00001)
       		md = min(md, dot(0.5*(mr+r)*stretch, normalize((r-mr)*stretch)));
    }

    return vec3(md, mc+n);
}

vec4 voronoi(vec2 uv, vec2 size, vec2 stretch, float intensity, float randomness, int seed) {
    uv *= size;
	vec3 v = iq_voronoi(uv, size, stretch, randomness, rand2(vec2(float(seed), 1.0-float(seed))));
	return vec4(v.yz, intensity*length((uv-v.yz)*stretch), v.x);
}
vec3 blend_normal(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*c1 + (1.0-opacity)*c2;
}

vec3 blend_dissolve(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	if (rand(uv) < opacity) {
		return c1;
	} else {
		return c2;
	}
}

vec3 blend_multiply(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*c1*c2 + (1.0-opacity)*c2;
}

vec3 blend_screen(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*(1.0-(1.0-c1)*(1.0-c2)) + (1.0-opacity)*c2;
}

float blend_overlay_f(float c1, float c2) {
	return (c1 < 0.5) ? (2.0*c1*c2) : (1.0-2.0*(1.0-c1)*(1.0-c2));
}

vec3 blend_overlay(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_overlay_f(c1.x, c2.x), blend_overlay_f(c1.y, c2.y), blend_overlay_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

vec3 blend_hard_light(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*0.5*(c1*c2+blend_overlay(uv, c1, c2, 1.0)) + (1.0-opacity)*c2;
}

float blend_soft_light_f(float c1, float c2) {
	return (c2 < 0.5) ? (2.0*c1*c2+c1*c1*(1.0-2.0*c2)) : 2.0*c1*(1.0-c2)+sqrt(c1)*(2.0*c2-1.0);
}

vec3 blend_soft_light(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_soft_light_f(c1.x, c2.x), blend_soft_light_f(c1.y, c2.y), blend_soft_light_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

float blend_burn_f(float c1, float c2) {
	return (c1==0.0)?c1:max((1.0-((1.0-c2)/c1)),0.0);
}

vec3 blend_burn(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_burn_f(c1.x, c2.x), blend_burn_f(c1.y, c2.y), blend_burn_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

float blend_dodge_f(float c1, float c2) {
	return (c1==1.0)?c1:min(c2/(1.0-c1),1.0);
}

vec3 blend_dodge(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*vec3(blend_dodge_f(c1.x, c2.x), blend_dodge_f(c1.y, c2.y), blend_dodge_f(c1.z, c2.z)) + (1.0-opacity)*c2;
}

vec3 blend_lighten(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*max(c1, c2) + (1.0-opacity)*c2;
}

vec3 blend_darken(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*min(c1, c2) + (1.0-opacity)*c2;
}

vec3 blend_difference(vec2 uv, vec3 c1, vec3 c2, float opacity) {
	return opacity*clamp(c2-c1, vec3(0.0), vec3(1.0)) + (1.0-opacity)*c2;
}
vec2 scale(vec2 uv, vec2 center, vec2 scale) {
	uv -= center;
	uv /= scale;
	uv += center;
    return uv;
}const float p_o2619577_default_in1 = 0.050000001;
const float p_o2619577_default_in2 = 0.000000000;
const float p_o3442925_cx = 0.000000000;
const float p_o3442925_cy = 0.000000000;
const float p_o3442925_scale_x = 2.099999905;
const float p_o3442925_scale_y = 2.099999905;
const float p_o2609904_default_in1 = 0.000000000;
const float p_o2609904_default_in2 = 0.000000000;
const float p_o1839956_default_in1 = 0.000000000;
const float p_o1839956_default_in2 = 1.000000000;
const float p_o1666169_amount = 1.000000000;
const float p_o3278853_cx = 0.000000000;
const float p_o3278853_cy = 0.000000000;
const float p_o1730094_translate_x = 0.000000000;
const float p_o1730094_translate_y = 0.000000000;
const float p_o1730094_rotate = 0.000000000;
const float p_o1730094_scale_x = 2.000000000;
const float p_o1730094_scale_y = 2.000000000;
const float p_o1711848_default_in1 = 0.000000000;
const float p_o1711848_default_in2 = 0.200000000;
const float p_o1053514_default_in1 = 0.000000000;
const float p_o1053514_default_in2 = 0.000000000;
const float p_o2016703_in_min = -0.280000000;
const float p_o2016703_in_max = 1.000000000;
const float p_o2016703_out_min = 0.000000000;
const float p_o2016703_out_max = 1.000000000;
const float p_o2003912_gradient_0_pos = 0.000000000;
const float p_o2003912_gradient_0_r = 0.000000000;
const float p_o2003912_gradient_0_g = 0.000000000;
const float p_o2003912_gradient_0_b = 0.000000000;
const float p_o2003912_gradient_0_a = 1.000000000;
const float p_o2003912_gradient_1_pos = 1.000000000;
const float p_o2003912_gradient_1_r = 1.000000000;
const float p_o2003912_gradient_1_g = 1.000000000;
const float p_o2003912_gradient_1_b = 1.000000000;
const float p_o2003912_gradient_1_a = 1.000000000;
vec4 o2003912_gradient_gradient_fct(float x) {
  if (x < p_o2003912_gradient_0_pos) {
    return vec4(p_o2003912_gradient_0_r,p_o2003912_gradient_0_g,p_o2003912_gradient_0_b,p_o2003912_gradient_0_a);
  } else if (x < p_o2003912_gradient_1_pos) {
    return mix(vec4(p_o2003912_gradient_0_r,p_o2003912_gradient_0_g,p_o2003912_gradient_0_b,p_o2003912_gradient_0_a), vec4(p_o2003912_gradient_1_r,p_o2003912_gradient_1_g,p_o2003912_gradient_1_b,p_o2003912_gradient_1_a), ((x-p_o2003912_gradient_0_pos)/(p_o2003912_gradient_1_pos-p_o2003912_gradient_0_pos)));
  }
  return vec4(p_o2003912_gradient_1_r,p_o2003912_gradient_1_g,p_o2003912_gradient_1_b,p_o2003912_gradient_1_a);
}
const float p_o1874989_default_in2 = 0.550000000;
const float p_o1646672_sides = 2.000000000;
const float p_o1646672_radius = 1.000000000;
const float p_o1646672_edge = 5.000000000;
const int seed_o1646669 = -20210;
const float p_o1646669_scale_x = 32.000000000;
const float p_o1646669_scale_y = 32.000000000;
const float p_o1646669_iterations = 8.000000000;
const float p_o1646669_persistence = 0.400000000;
const float p_o1227913_amount = 1.000000000;
const float p_o1227347_sides = 2.000000000;
const float p_o1227347_radius = 1.000000000;
const float p_o1227347_edge = 1.000000000;
const float p_o3232740_cx = 0.000000000;
const float p_o3232740_cy = 0.000000000;
const int seed_o1383595 = -22407;
const float p_o1383595_scale_x = 7.000000000;
const float p_o1383595_scale_y = 7.000000000;
const float p_o1383595_iterations = 2.000000000;
const float p_o1383595_persistence = 0.950000000;
const float p_o1170149_radius = 1.000000000;
const float p_o1170149_repeat = 1.000000000;
const float p_o1175160_cx = 0.000000000;
const float p_o1175160_cy = 0.000000000;
const float p_o1175160_rotate = 180.000000000;
const float p_o3180989_translate_y = 0.000000000;
const float p_o1107567_amount = 1.000000000;
const float p_o1115718_translate_x = 0.000000000;
const float p_o1115718_translate_y = -0.649999976;
const float p_o1115718_rotate = 0.000000000;
const float p_o1115718_scale_x = 1.000000000;
const float p_o1115718_scale_y = 1.000000000;
const int seed_o1098035 = -53939;
const float p_o1098035_scale_x = 64.000000000;
const float p_o1098035_scale_y = 6.000000000;
const float p_o1098035_stretch_x = 1.000000000;
const float p_o1098035_stretch_y = 1.000000000;
const float p_o1098035_intensity = 1.000000000;
const float p_o1098035_randomness = 0.850000000;
const float p_o1135497_gradient_0_pos = 0.718181818;
const float p_o1135497_gradient_0_r = 0.000000000;
const float p_o1135497_gradient_0_g = 0.000000000;
const float p_o1135497_gradient_0_b = 0.000000000;
const float p_o1135497_gradient_0_a = 1.000000000;
const float p_o1135497_gradient_1_pos = 0.918181818;
const float p_o1135497_gradient_1_r = 1.000000000;
const float p_o1135497_gradient_1_g = 1.000000000;
const float p_o1135497_gradient_1_b = 1.000000000;
const float p_o1135497_gradient_1_a = 1.000000000;
const float p_o1135497_gradient_2_pos = 0.945454545;
const float p_o1135497_gradient_2_r = 0.000000000;
const float p_o1135497_gradient_2_g = 0.000000000;
const float p_o1135497_gradient_2_b = 0.000000000;
const float p_o1135497_gradient_2_a = 1.000000000;
vec4 o1135497_gradient_gradient_fct(float x) {
  if (x < p_o1135497_gradient_0_pos) {
    return vec4(p_o1135497_gradient_0_r,p_o1135497_gradient_0_g,p_o1135497_gradient_0_b,p_o1135497_gradient_0_a);
  } else if (x < p_o1135497_gradient_1_pos) {
    return mix(vec4(p_o1135497_gradient_0_r,p_o1135497_gradient_0_g,p_o1135497_gradient_0_b,p_o1135497_gradient_0_a), vec4(p_o1135497_gradient_1_r,p_o1135497_gradient_1_g,p_o1135497_gradient_1_b,p_o1135497_gradient_1_a), ((x-p_o1135497_gradient_0_pos)/(p_o1135497_gradient_1_pos-p_o1135497_gradient_0_pos)));
  } else if (x < p_o1135497_gradient_2_pos) {
    return mix(vec4(p_o1135497_gradient_1_r,p_o1135497_gradient_1_g,p_o1135497_gradient_1_b,p_o1135497_gradient_1_a), vec4(p_o1135497_gradient_2_r,p_o1135497_gradient_2_g,p_o1135497_gradient_2_b,p_o1135497_gradient_2_a), ((x-p_o1135497_gradient_1_pos)/(p_o1135497_gradient_2_pos-p_o1135497_gradient_1_pos)));
  }
  return vec4(p_o1135497_gradient_2_r,p_o1135497_gradient_2_g,p_o1135497_gradient_2_b,p_o1135497_gradient_2_a);
}
const float p_o1147502_in_min = -1.950000000;
const float p_o1147502_in_max = 2.510000000;
const float p_o1147502_out_min = 0.080000000;
const float p_o1147502_out_max = 2.000000000;
const float p_o1079786_default_in1 = 0.000000000;
const float p_o1079786_default_in2 = 0.000000000;
const float p_o1544215_amount = 0.340000000;
const float p_o3153493_translate_y = 0.000000000;
const int seed_o1541301 = -32562;
const float p_o1541301_scale_x = 12.000000000;
const float p_o1541301_scale_y = 1.000000000;
const float p_o1541301_iterations = 1.000000000;
const float p_o1541301_persistence = 0.500000000;
const float p_o1047621_repeat = 1.000000000;
const float p_o1047621_rotate = -90.000000000;
const float p_o1047621_gradient_0_pos = 0.000000000;
const float p_o1047621_gradient_0_r = 0.000000000;
const float p_o1047621_gradient_0_g = 0.000000000;
const float p_o1047621_gradient_0_b = 0.000000000;
const float p_o1047621_gradient_0_a = 1.000000000;
const float p_o1047621_gradient_1_pos = 1.000000000;
const float p_o1047621_gradient_1_r = 1.000000000;
const float p_o1047621_gradient_1_g = 1.000000000;
const float p_o1047621_gradient_1_b = 1.000000000;
const float p_o1047621_gradient_1_a = 1.000000000;
vec4 o1047621_gradient_gradient_fct(float x) {
  if (x < p_o1047621_gradient_0_pos) {
    return vec4(p_o1047621_gradient_0_r,p_o1047621_gradient_0_g,p_o1047621_gradient_0_b,p_o1047621_gradient_0_a);
  } else if (x < p_o1047621_gradient_1_pos) {
    return mix(vec4(p_o1047621_gradient_0_r,p_o1047621_gradient_0_g,p_o1047621_gradient_0_b,p_o1047621_gradient_0_a), vec4(p_o1047621_gradient_1_r,p_o1047621_gradient_1_g,p_o1047621_gradient_1_b,p_o1047621_gradient_1_a), ((x-p_o1047621_gradient_0_pos)/(p_o1047621_gradient_1_pos-p_o1047621_gradient_0_pos)));
  }
  return vec4(p_o1047621_gradient_1_r,p_o1047621_gradient_1_g,p_o1047621_gradient_1_b,p_o1047621_gradient_1_a);
}
const float p_o2313099_curve_0_x = 0.000000000;
const float p_o2313099_curve_0_y = 0.887122393;
const float p_o2313099_curve_0_ls = 0.000000000;
const float p_o2313099_curve_0_rs = -0.653084097;
const float p_o2313099_curve_1_x = 0.451992810;
const float p_o2313099_curve_1_y = 0.484896600;
const float p_o2313099_curve_1_ls = -0.936088853;
const float p_o2313099_curve_1_rs = -1.978554322;
const float p_o2313099_curve_2_x = 1.000000000;
const float p_o2313099_curve_2_y = 0.000000000;
const float p_o2313099_curve_2_ls = 0.068362479;
const float p_o2313099_curve_2_rs = -0.000000000;
float o2313099_curve_curve_fct(float x) {
if (x <= p_o2313099_curve_1_x) {
float dx = x - p_o2313099_curve_0_x;
float d = p_o2313099_curve_1_x - p_o2313099_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2313099_curve_0_y;
float yac = p_o2313099_curve_0_y + d*p_o2313099_curve_0_rs;
float ybc = p_o2313099_curve_1_y - d*p_o2313099_curve_1_ls;
float y2 = p_o2313099_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o2313099_curve_2_x) {
float dx = x - p_o2313099_curve_1_x;
float d = p_o2313099_curve_2_x - p_o2313099_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2313099_curve_1_y;
float yac = p_o2313099_curve_1_y + d*p_o2313099_curve_1_rs;
float ybc = p_o2313099_curve_2_y - d*p_o2313099_curve_2_ls;
float y2 = p_o2313099_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}
const float p_o2047992_curve_0_x = 0.000000000;
const float p_o2047992_curve_0_y = 1.000000000;
const float p_o2047992_curve_0_ls = 0.000000000;
const float p_o2047992_curve_0_rs = -37.443563156;
const float p_o2047992_curve_1_x = 0.045742754;
const float p_o2047992_curve_1_y = 0.004769504;
const float p_o2047992_curve_1_ls = 0.000000000;
const float p_o2047992_curve_1_rs = -0.000000000;
const float p_o2047992_curve_2_x = 0.202898547;
const float p_o2047992_curve_2_y = 0.069952309;
const float p_o2047992_curve_2_ls = 1.276485086;
const float p_o2047992_curve_2_rs = 0.566182865;
const float p_o2047992_curve_3_x = 0.633605063;
const float p_o2047992_curve_3_y = 0.527821898;
const float p_o2047992_curve_3_ls = 2.990280303;
const float p_o2047992_curve_3_rs = 3.026153690;
const float p_o2047992_curve_4_x = 1.000000000;
const float p_o2047992_curve_4_y = 1.000000000;
const float p_o2047992_curve_4_ls = 0.266607613;
const float p_o2047992_curve_4_rs = -0.000000000;
float o2047992_curve_curve_fct(float x) {
if (x <= p_o2047992_curve_1_x) {
float dx = x - p_o2047992_curve_0_x;
float d = p_o2047992_curve_1_x - p_o2047992_curve_0_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2047992_curve_0_y;
float yac = p_o2047992_curve_0_y + d*p_o2047992_curve_0_rs;
float ybc = p_o2047992_curve_1_y - d*p_o2047992_curve_1_ls;
float y2 = p_o2047992_curve_1_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o2047992_curve_2_x) {
float dx = x - p_o2047992_curve_1_x;
float d = p_o2047992_curve_2_x - p_o2047992_curve_1_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2047992_curve_1_y;
float yac = p_o2047992_curve_1_y + d*p_o2047992_curve_1_rs;
float ybc = p_o2047992_curve_2_y - d*p_o2047992_curve_2_ls;
float y2 = p_o2047992_curve_2_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o2047992_curve_3_x) {
float dx = x - p_o2047992_curve_2_x;
float d = p_o2047992_curve_3_x - p_o2047992_curve_2_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2047992_curve_2_y;
float yac = p_o2047992_curve_2_y + d*p_o2047992_curve_2_rs;
float ybc = p_o2047992_curve_3_y - d*p_o2047992_curve_3_ls;
float y2 = p_o2047992_curve_3_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
if (x <= p_o2047992_curve_4_x) {
float dx = x - p_o2047992_curve_3_x;
float d = p_o2047992_curve_4_x - p_o2047992_curve_3_x;
float t = dx/d;
float omt = (1.0 - t);
float omt2 = omt * omt;
float omt3 = omt2 * omt;
float t2 = t * t;
float t3 = t2 * t;
d /= 3.0;
float y1 = p_o2047992_curve_3_y;
float yac = p_o2047992_curve_3_y + d*p_o2047992_curve_3_rs;
float ybc = p_o2047992_curve_4_y - d*p_o2047992_curve_4_ls;
float y2 = p_o2047992_curve_4_y;
return y1*omt3 + yac*omt2*t*3.0 + ybc*omt*t2*3.0 + y2*t3;
}
}

void fragment() {
float o1874989_0_clamp_false = (o3007790_ANIM)*p_o1874989_default_in2;
float o1874989_0_clamp_true = clamp(o1874989_0_clamp_false, 0.0, 1.0);
float o1874989_0_2_f = o1874989_0_clamp_false;
vec4 o2003912_0_1_rgba = o2003912_gradient_gradient_fct(o1874989_0_2_f);
vec4 o2016703_0_1_rgba = vec4(vec3(p_o2016703_out_min)+(o2003912_0_1_rgba.rgb-vec3(p_o2016703_in_min))*vec3((p_o2016703_out_max-p_o2016703_out_min)/(p_o2016703_in_max-p_o2016703_in_min)), o2003912_0_1_rgba.a);
float o1053514_0_clamp_false = abs((dot((o2016703_0_1_rgba).rgb, vec3(1.0))/3.0));
float o1053514_0_clamp_true = clamp(o1053514_0_clamp_false, 0.0, 1.0);
float o1053514_0_1_f = o1053514_0_clamp_true;
float o1711848_0_clamp_false = o1053514_0_1_f+p_o1711848_default_in2;
float o1711848_0_clamp_true = clamp(o1711848_0_clamp_false, 0.0, 1.0);
float o1711848_0_2_f = o1711848_0_clamp_false;
float o1646669_0_1_f = perlin((transform2_clamp(transform2((rotate((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), vec2(0.5+p_o3278853_cx, 0.5+p_o3278853_cy), (o3007790_OFFSET * 530.0)*0.01745329251)), vec2(p_o1730094_translate_x*(2.0*1.0-1.0), p_o1730094_translate_y*(2.0*1.0-1.0)), p_o1730094_rotate*0.01745329251*(2.0*1.0-1.0), vec2(p_o1730094_scale_x*(2.0*o1711848_0_2_f-1.0), p_o1730094_scale_y*(2.0*o1711848_0_2_f-1.0))))), vec2(p_o1646669_scale_x, p_o1646669_scale_y), int(p_o1646669_iterations), p_o1646669_persistence, seed_o1646669);
float o1646672_0_1_f = shape_circle((transform2_clamp(transform2((rotate((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), vec2(0.5+p_o3278853_cx, 0.5+p_o3278853_cy), (o3007790_OFFSET * 530.0)*0.01745329251)), vec2(p_o1730094_translate_x*(2.0*1.0-1.0), p_o1730094_translate_y*(2.0*1.0-1.0)), p_o1730094_rotate*0.01745329251*(2.0*1.0-1.0), vec2(p_o1730094_scale_x*(2.0*o1711848_0_2_f-1.0), p_o1730094_scale_y*(2.0*o1711848_0_2_f-1.0))))), p_o1646672_sides, p_o1646672_radius*1.0, p_o1646672_edge*o1646669_0_1_f);
vec4 o1730094_0_1_rgba = vec4(vec3(o1646672_0_1_f), 1.0);
vec4 o3278853_0_1_rgba = o1730094_0_1_rgba;
float o1383595_0_1_f = perlin((rotate((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), vec2(0.5+p_o3232740_cx, 0.5+p_o3232740_cy), (o3007790_OFFSET * 180.0)*0.01745329251)), vec2(p_o1383595_scale_x, p_o1383595_scale_y), int(p_o1383595_iterations), p_o1383595_persistence, seed_o1383595);
vec4 o3232740_0_1_rgba = vec4(vec3(o1383595_0_1_f), 1.0);
float o1227347_0_1_f = shape_circle((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), p_o1227347_sides, p_o1227347_radius*(dot((o3232740_0_1_rgba).rgb, vec3(1.0))/3.0), p_o1227347_edge*1.0);
float o1874989_3_clamp_false = (o3007790_ANIM)*p_o1874989_default_in2;
float o1874989_3_clamp_true = clamp(o1874989_3_clamp_false, 0.0, 1.0);
float o1874989_0_5_f = o1874989_3_clamp_false;
vec4 o2003912_0_3_rgba = o2003912_gradient_gradient_fct(o1874989_0_5_f);
vec4 o2016703_0_3_rgba = vec4(vec3(p_o2016703_out_min)+(o2003912_0_3_rgba.rgb-vec3(p_o2016703_in_min))*vec3((p_o2016703_out_max-p_o2016703_out_min)/(p_o2016703_in_max-p_o2016703_in_min)), o2003912_0_3_rgba.a);
float o1053514_2_clamp_false = abs((dot((o2016703_0_3_rgba).rgb, vec3(1.0))/3.0));
float o1053514_2_clamp_true = clamp(o1053514_2_clamp_false, 0.0, 1.0);
float o1053514_0_3_f = o1053514_2_clamp_true;
vec4 o1098035_0_xyzw = voronoi((fract(transform2(((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y)), vec2(p_o1115718_translate_x*(2.0*1.0-1.0), p_o1115718_translate_y*(2.0*o1053514_0_3_f-1.0)), p_o1115718_rotate*0.01745329251*(2.0*1.0-1.0), vec2(p_o1115718_scale_x*(2.0*1.0-1.0), p_o1115718_scale_y*(2.0*1.0-1.0))))), vec2(p_o1098035_scale_x, p_o1098035_scale_y), vec2(p_o1098035_stretch_y, p_o1098035_stretch_x), p_o1098035_intensity, p_o1098035_randomness, seed_o1098035);float o1098035_0_1_f = o1098035_0_xyzw.z;
vec4 o1099342_0_1_rgba = vec4(vec3(1.0)-vec4(vec3(o1098035_0_1_f), 1.0).rgb, vec4(vec3(o1098035_0_1_f), 1.0).a);
vec4 o1115718_0_1_rgba = o1099342_0_1_rgba;
float o1541301_0_1_f = perlin((((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y))-vec2((o3007790_OFFSET * 3.5), p_o3153493_translate_y)), vec2(p_o1541301_scale_x, p_o1541301_scale_y), int(p_o1541301_iterations), p_o1541301_persistence, seed_o1541301);
vec4 o3153493_0_1_rgba = vec4(vec3(o1541301_0_1_f), 1.0);
float o1047621_0_r = 0.5+(cos(p_o1047621_rotate*0.01745329251)*(((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y)).x-0.5)+sin(p_o1047621_rotate*0.01745329251)*(((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y)).y-0.5))/(cos(abs(mod(p_o1047621_rotate, 90.0)-45.0)*0.01745329251)*1.41421356237);vec4 o1047621_0_1_rgba = o1047621_gradient_gradient_fct(fract(o1047621_0_r*p_o1047621_repeat));
vec4 o1544215_0_s1 = o3153493_0_1_rgba;
vec4 o1544215_0_s2 = o1047621_0_1_rgba;
float o1544215_0_a = p_o1544215_amount*1.0;
vec4 o1544215_0_2_rgba = vec4(blend_normal(((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y)), o1544215_0_s1.rgb, o1544215_0_s2.rgb, o1544215_0_a*o1544215_0_s1.a), min(1.0, o1544215_0_s2.a+o1544215_0_a*o1544215_0_s1.a));
float o1079786_0_clamp_false = (dot((o1544215_0_2_rgba).rgb, vec3(1.0))/3.0)-o1053514_0_3_f;
float o1079786_0_clamp_true = clamp(o1079786_0_clamp_false, 0.0, 1.0);
float o1079786_0_1_f = o1079786_0_clamp_false;
vec4 o1147502_0_1_rgba = vec4(vec3(p_o1147502_out_min)+(vec4(vec3(o1079786_0_1_f), 1.0).rgb-vec3(p_o1147502_in_min))*vec3((p_o1147502_out_max-p_o1147502_out_min)/(p_o1147502_in_max-p_o1147502_in_min)), vec4(vec3(o1079786_0_1_f), 1.0).a);
vec4 o1135497_0_1_rgba = o1135497_gradient_gradient_fct((dot((o1147502_0_1_rgba).rgb, vec3(1.0))/3.0));
vec4 o1107567_0_s1 = o1115718_0_1_rgba;
vec4 o1107567_0_s2 = o1135497_0_1_rgba;
float o1107567_0_a = p_o1107567_amount*1.0;
vec4 o1107567_0_2_rgba = vec4(blend_multiply(((rotate((vec2(fract(p_o1170149_repeat*atan((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).y-0.5, (scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))).x-0.5)*0.15915494309), min(0.99999, 2.0/p_o1170149_radius*length((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y)))-vec2(0.5))))), vec2(0.5+p_o1175160_cx, 0.5+p_o1175160_cy), p_o1175160_rotate*0.01745329251))-vec2((o3007790_OFFSET * 1.32), p_o3180989_translate_y)), o1107567_0_s1.rgb, o1107567_0_s2.rgb, o1107567_0_a*o1107567_0_s1.a), min(1.0, o1107567_0_s2.a+o1107567_0_a*o1107567_0_s1.a));
vec4 o3180989_0_1_rgba = o1107567_0_2_rgba;
vec4 o1175160_0_1_rgba = o3180989_0_1_rgba;
vec4 o1170149_0_1_rgba = o1175160_0_1_rgba;
vec4 o1227913_0_s1 = vec4(vec3(o1227347_0_1_f), 1.0);
vec4 o1227913_0_s2 = o1170149_0_1_rgba;
float o1227913_0_a = p_o1227913_amount*1.0;
vec4 o1227913_0_2_rgba = vec4(blend_multiply((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), o1227913_0_s1.rgb, o1227913_0_s2.rgb, o1227913_0_a*o1227913_0_s1.a), min(1.0, o1227913_0_s2.a+o1227913_0_a*o1227913_0_s1.a));
float o1874989_6_clamp_false = (o3007790_ANIM)*p_o1874989_default_in2;
float o1874989_6_clamp_true = clamp(o1874989_6_clamp_false, 0.0, 1.0);
float o1874989_0_8_f = o1874989_6_clamp_false;
vec4 o2003912_0_5_rgba = o2003912_gradient_gradient_fct(o1874989_0_8_f);
vec4 o2016703_0_5_rgba = vec4(vec3(p_o2016703_out_min)+(o2003912_0_5_rgba.rgb-vec3(p_o2016703_in_min))*vec3((p_o2016703_out_max-p_o2016703_out_min)/(p_o2016703_in_max-p_o2016703_in_min)), o2003912_0_5_rgba.a);
float o1053514_4_clamp_false = abs((dot((o2016703_0_5_rgba).rgb, vec3(1.0))/3.0));
float o1053514_4_clamp_true = clamp(o1053514_4_clamp_false, 0.0, 1.0);
float o1053514_0_5_f = o1053514_4_clamp_true;
float o2313099_0_1_f = o2313099_curve_curve_fct(o1053514_0_5_f);
vec4 o1666169_0_s1 = o3278853_0_1_rgba;
vec4 o1666169_0_s2 = o1227913_0_2_rgba;
float o1666169_0_a = p_o1666169_amount*o2313099_0_1_f;
vec4 o1666169_0_1_rgba = vec4(blend_normal((scale((UV), vec2(0.5+p_o3442925_cx, 0.5+p_o3442925_cy), vec2(p_o3442925_scale_x, p_o3442925_scale_y))), o1666169_0_s1.rgb, o1666169_0_s2.rgb, o1666169_0_a*o1666169_0_s1.a), min(1.0, o1666169_0_s2.a+o1666169_0_a*o1666169_0_s1.a));
float o1839956_0_clamp_false = (dot((o1666169_0_1_rgba).rgb, vec3(1.0))/3.0)*p_o1839956_default_in2;
float o1839956_0_clamp_true = clamp(o1839956_0_clamp_false, 0.0, 1.0);
float o1839956_0_2_f = o1839956_0_clamp_true;
float o2047992_0_1_f = o2047992_curve_curve_fct(o1053514_0_5_f);
float o2609904_0_clamp_false = o1839956_0_2_f-o2047992_0_1_f;
float o2609904_0_clamp_true = clamp(o2609904_0_clamp_false, 0.0, 1.0);
float o2609904_0_1_f = o2609904_0_clamp_false;
vec4 o3442925_0_1_rgba = vec4(vec3(o2609904_0_1_f), 1.0);
float o2619577_0_clamp_false = step(p_o2619577_default_in1,(dot((o3442925_0_1_rgba).rgb, vec3(1.0))/3.0));
float o2619577_0_clamp_true = clamp(o2619577_0_clamp_false, 0.0, 1.0);
float o2619577_0_2_f = o2619577_0_clamp_true;
ALBEDO = vec3(0.7, 0, 0);
ROUGHNESS = 0.0;
ALPHA = o2619577_0_2_f;
}
