@tool
extends Area3D

var owner_pid: int

var bodies_processed = []

@export var damage : int
@export var push : int
@export var blast_radius : float = 8.0:
	set(value): # also runs in editor!
		blast_radius = value
		if Engine.is_editor_hint(): # this must only be triggered in editor!
			apply_radius()
			
@export var blast_duration : float = 0.05

func apply_radius() -> void:
	$BlastRadius.shape.radius = blast_radius
	$MeshInstance3D.mesh.radius = blast_radius
	$MeshInstance3D.mesh.height = blast_radius * 2

func _ready() -> void:
	if Engine.is_editor_hint():
		return
		
	apply_radius()
	$Timer.start(blast_duration)

func _process(delta: float) -> void:
	if Engine.is_editor_hint():
		return
		
	if damage != 0 and owner_pid != 0:
		monitoring = true
	
func give_damage(target: Node, hit_position: Vector3, hit_normal: Vector3, damage: int, source_position: Vector3, type: Globals.DamageType, push: float) -> void:
	if not is_multiplayer_authority():
		print_debug("Attempting to deal damage from a puppet. Ignoring")
		return
	if target != null:
		if target.has_method(&'take_damage'): # we've hit a player or something else - they will handle everything like effects etc.
#			print("Giving explosion blast damage to ", target)
			target.rpc(&'take_damage', owner_pid, hit_position, hit_normal, damage, source_position, type, push)
#		else:
#			print("Target has no take_damage method")
#	elif target == null:
#		print ("Target is null")

func _on_explosion_body_entered(body) -> void:
	if Engine.is_editor_hint():
		return
#	print("Processing ", body)
	if body not in bodies_processed:
#	print(body, " has not been processed yet, giving damage now")
		give_damage(body, body.global_transform.origin, Vector3.DOWN, damage, global_transform.origin, Globals.DamageType.EXPLOSION, push)
		bodies_processed.append(body)
#	else:
#		print(body, " was already processed")
